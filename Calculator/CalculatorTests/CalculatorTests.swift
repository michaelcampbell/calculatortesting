//
//  CalculatorTests.swift
//  CalculatorTests
//
//  Created by Michael Campbell on 2/7/16.
//  Copyright © 2016 Prolific Interactive. All rights reserved.
//

import XCTest
@testable import Calculator

class CalculatorTests: XCTestCase {
    
    var calculatorBrain: CalculatorBrain!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        calculatorBrain = CalculatorBrain()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        calculatorBrain = nil
    }
    
    // MARK: Addition
    
    func test_addingTwoZeroesEqualsZero() {
        // given
        let firstValue: Int32 = 0
        let secondValue: Int32 = 0
        let expectation: Int32 = 0
        
        // when
        let result = calculatorBrain.addNumber(firstValue, andSecondNumber: secondValue)
        
        // then
        XCTAssertEqual(result, expectation, "Expected result = \(expectation), got \(result)")
    }
    
    func test_adding5And6Is11() {
        // given
        let firstValue: Int32 = 5
        let secondValue: Int32 = 6
        let expectation: Int32 = 11
        
        // when
        let result = calculatorBrain.addNumber(firstValue, andSecondNumber: secondValue)
        
        // then
        XCTAssertEqual(result, expectation, "Expected result = \(expectation), got \(result)")
    }
    
    func test_addingLargeValues() {
        // given
        let firstValue: Int32 = 89457634
        let secondValue: Int32 = 238746237
        let expectation: Int32 = 328203871
        
        // when
        let result = calculatorBrain.addNumber(firstValue, andSecondNumber: secondValue)
        
        // then
        XCTAssertEqual(result, expectation, "Expected result = \(expectation), got \(result)")
    }
    
    // MARK: Subtraction
    
    func test_subtractingTwoZeroesEqualsZero() {
        
    }
    
    func test_subtracting9From4IsNegative() {
        
    }
    
    func test_subtracting5From10Equals5() {
        
    }
    
    // MARK: Multiplication
    
    // MARK: Division
    
}
