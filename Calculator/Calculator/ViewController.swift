//
//  ViewController.swift
//  Calculator
//
//  Created by Michael Campbell on 2/7/16.
//  Copyright © 2016 Prolific Interactive. All rights reserved.
//

import UIKit

internal class ViewController: UIViewController {
    
    @IBOutlet private weak var calculatorScreen: UILabel!
    
    private var calculatorBrain: CalculatorBrain?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calculatorBrain = CalculatorBrain()
        calculatorBrain?.delegate = self
        
        resetScreen()
    }
    
    func resetScreen() {
        calculatorBrain?.resetDisplay()
    }
    
    @IBAction func tappedNumber(sender: UIButton) {
        guard let currentTitle = sender.currentTitle else {
            return
        }
        
        calculatorBrain?.didTapNumberWithValue(currentTitle)
    }
    
    @IBAction func tappedOperator(sender: UIButton) {
        guard let currentTitle = sender.currentTitle else {
            return
        }
        
        calculatorBrain?.didTapOperatorWithValue(currentTitle)
    }
    
    @IBAction func tappedEqual(sender: UIButton) {
        calculatorBrain?.didTapEqual()
    }
    
    @IBAction func tappedClear(sender: UIButton) {
        resetScreen()
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}

// MARK: CalculatorBrainDelegate

extension ViewController: CalculatorBrainDelegate {
    
    func didUpdateDisplayWithText(displayText: String) {
        calculatorScreen.text = displayText
    }
    
}

