//
//  CalculatorBrain.h
//  CalculatorBrain
//
//  Created by Horrible Engineer on 2/11/15.
//  Copyright © 2015 Super Calculator Co. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CalculatorBrain.
FOUNDATION_EXPORT double CalculatorBrainVersionNumber;

//! Project version string for CalculatorBrain.
FOUNDATION_EXPORT const unsigned char CalculatorBrainVersionString[];

@protocol CalculatorBrainDelegate <NSObject>

- (void)didUpdateDisplayWithText:(nonnull NSString *)displayText;

@end

@interface CalculatorBrain : NSObject

/**
 *  The calculator brain delegate
 */
@property (nonatomic, weak) id<CalculatorBrainDelegate> delegate;

/**
 *  Resets the Display
 */
- (void)resetDisplay;

/**
 *  Should be called when a number was tapped
 *
 *  @param value The value that was tapped
 */
- (void)didTapNumberWithValue:(nonnull NSString *)value;


/**
 *  Should be called when an operator was tapped
 *
 *  @param value The operator that was tapped
 */
- (void)didTapOperatorWithValue:(nonnull NSString *)value;

/**
 *  Should be called when the equal sign was  tapped
 */
- (void)didTapEqual;

/**
 *  Adds two numbers
 *
 *  @param first  (int) number
 *  @param second (int) number
 *
 *  @return (int) sum of first and second numbers
 */
- (int)addNumber:(int)first andSecondNumber:(int)second;

/**
 *  Subtracts two numbers
 *
 *  @param first  (int) number
 *  @param second (int) number
 *
 *  @return (int) difference of first and second numbers
 */
- (int)subtractNumber:(int)first andSecondNumber:(int)second;

/**
 *  Divides two numbers
 *
 *  @param first  (int) number
 *  @param second (int) number
 *
 *  @return (int) quotient of first and second numbers
 */
- (int)divideNumber:(int)first andSecondNumber:(int)second;

/**
 *  Multiplies two numbers
 *
 *  @param first  (int) number
 *  @param second (int) number
 *
 *  @return (int) product of first and second numbers
 */
- (int)multiplyNumber:(int)first andSecondNumber:(int)second;

@end
